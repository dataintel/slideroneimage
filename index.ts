import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetSliderOneImageComponent } from './src/base-widget.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetSliderOneImageComponent
  ],
  exports: [
    BaseWidgetSliderOneImageComponent
  ]
})
export class SliderOneImageModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SliderOneImageModule,
      providers: []
    };
  }
}
