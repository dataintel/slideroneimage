import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slideroneimage',
  template: `

  <style>
  /* Makes images fully responsive */

.img-responsive,
.thumbnail > img,
.thumbnail a > img,
.carousel-inner > .item > img,
.carousel-inner > .item > a > img {
  display: block;
  width: 100%;
  height: auto;
}

/* ------------------- Carousel Styling ------------------- */

.carousel-inner {
  width: 100%;
}

.carousel-caption {
  background-color: rgba(0,0,0,.5);
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  padding: 5px 0 5px 0;
  color: #fff;
  text-align: center;
  width: 100%;
  font-weight: bold;
  margin-bottom: 10px;
  line-height: 13px;
  font-size: 11px;
}

.carousel-caption2 {
  background-color: rgba(0,0,0,.5);
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  padding: 5px 0 5px 0;
  color: #fff;
  text-align: center;
  width: 100%;
  font-weight: bold;
}

.carousel-indicators {
  position: absolute;
  bottom: 0;
  right: 0;
  left: 0;
  width: 100%;
  z-index: 15;
  margin: 0;
  padding: 0 25px 25px 0;
  text-align: right;
}

.carousel-control.left,
.carousel-control.right {
  background-image: none;
}


/* ------------------- Section Styling - Not needed for carousel styling ------------------- */

.section-white {
   padding: 10px 0;
}

.section-white {
  background-color: #fff;
  color: #555;
}

@media screen and (min-width: 768px) {

  .section-white {
     padding: 1.5em 0;
  }

}

@media screen and (min-width: 992px) {

  .container {
    max-width: 930px;
    width: 18%;
  }

}

.title{
    font-size:15px;
    color:red;
    margin-left:-20px;
    font-weight: bold;
    margin-bottom: 15px;
}

.line-separator{
    height:1px;
    width:650px;
    background: #A9A9A9;
    margin-bottom:65px;
    margin-left:-30px;
}
  </style>
  <section class="section-white">
  <div class="container">
      <div class=title>
        My Topics
      </div>
  <div class="line-separator"></div>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <!--<div ngClass="'item active' : image.title==1, 'item' : image.title!==1">-->
        <div *ngFor="let image of images" [ngClass]="{'item active':image.title==1, 'item':image.title!==1}">
          <img src="{{image.n_image}}" alt="...">
          <div class="carousel-caption">
            {{image.n_title}}
          </div>
          <div class="carousel-caption2">
            {{image.ptitle}}
          </div>
        </div>
      </div>
      <!-- Controls -->
    </div>
  </div>
</section>`
})
export class BaseWidgetSliderOneImageComponent implements OnInit {

  public images: Array<any> = [
    { title: "1", ptitle: "News 1", n_title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    n_image: "./src/assets/covered.jpg" },
    { title: "2", ptitle: "News 2", n_title: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s ",
    n_image: "./src/assets/generation.jpg" },
    { title: "3", ptitle: "News 3", n_title: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. ",
    n_image: "./src/assets/potter.jpg" },
    { title: "4", ptitle: "News 4", n_title: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
    n_image: "./src/assets/preschool.jpg" },
    { title: "5", ptitle: "News 5", n_title: "Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.",
    n_image: "./src/assets/soccer.jpg" }
  ];

  constructor() { }

  ngOnInit() {
  }

}